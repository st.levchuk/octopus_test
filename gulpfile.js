'use strict';

var gulp = require('gulp'),
  watch = require('gulp-watch'),
  prefixer = require('gulp-autoprefixer'),
  uglify = require('gulp-uglify'),
  sass = require('gulp-sass'),
  sourcemaps = require('gulp-sourcemaps'),
  rigger = require('gulp-rigger'),
  cleanCSS = require('gulp-clean-css'),
  imagemin = require('gulp-imagemin'),
  pngquant = require('imagemin-pngquant'),
  rimraf = require('rimraf'),
  spritesmith = require('gulp.spritesmith'),
  browserSync = require("browser-sync"),
  reload = browserSync.reload,
  svgSprite = require('gulp-svg-sprite'),
  gulpSequence = require('gulp-sequence');
var path = {
  build: {
    html: 'build/',
    js: 'build/js/',
    css: 'build/css/',
    img: 'build/img/',
    fonts: 'build/fonts/',
    ajax: 'build/ajax/',
    php: 'build/php/',
    video: 'build/video/',
    json: 'build/',
    xml: 'build/',
    sprite: 'build/img/'
  },
  src: {
    html: 'src/*.html',
    js: 'src/js/*.js',
    style: 'src/style/*.scss',
    img: 'src/img/**/*.{gif,jpg,png,svg,ico}',
    fonts: 'src/fonts/**/*.{eot,woff,woff2,svg}',
    ajax: 'src/ajax/**/*.*',
    php: 'src/php/**/*.*',
    video: 'src/video/**/*.*',
    json: 'src/*.json',
    xml: 'src/*.xml'
  },
  watch: {
    html: 'src/**/*.html',
    js: 'src/js/**/*.js',
    style: 'src/style/**/*.scss',
    img: 'src/img/**/*.{gif,jpg,png,svg,ico}',
    fonts: 'src/fonts/**/*.{eot,woff,woff2,svg}',
    sprite: './src/sprite/*.*',
    spriteSvg: './src/svg-sprites/*.*',
    ajax: 'src/ajax/**/*.*',
    php: 'src/php/**/*.*',
    video: 'src/video/**/*.*',
    json: 'src/**/*.json',
    xml: 'xml/**/*.xml'
  },
  clean: './build'
};


var config = {
  server: {
    baseDir: "./build"
  },
  tunnel: true,
  host: 'localhost',
  port: 9000,
  logPrefix: "nordman_818"
};

gulp.task('webserver', function () {
  browserSync(config);
});

gulp.task('clean', function (cb) {
  rimraf(path.clean, cb);
});

gulp.task('html:build', function () {
  gulp.src(path.src.html)
    .pipe(rigger())
    .pipe(gulp.dest(path.build.html))
    .pipe(reload({stream: true}));
});

gulp.task('js:build', function () {
  gulp.src(path.src.js)
    .pipe(rigger())
    // .pipe(sourcemaps.init())
    .pipe(uglify())
    // .pipe(sourcemaps.write())
    .pipe(gulp.dest(path.build.js))
    .pipe(reload({stream: true}));
});

gulp.task('style:build', function () {
  gulp.src(path.src.style)
  // .pipe(sourcemaps.init())
    .pipe(sass({
      sourceMap: false,
      errLogToConsole: true
    }))
    .pipe(prefixer({
      browsers: [
        'ie >= 11',
        'ie_mob >= 11',
        'ff >= 30',
        'chrome >= 34',
        'safari >= 7',
        'opera >= 23',
        'ios >= 6',
        'android >= 4.4',
        'bb >= 10'
      ],
      cascade: false
    }))
    .pipe(cleanCSS())
    // .pipe(sourcemaps.write())
    .pipe(gulp.dest(path.build.css))
    .pipe(reload({stream: true}));
});

gulp.task('image:build', function () {
  return gulp.src(path.src.img)
    .pipe(imagemin({
      progressive: true,
      interlaced: true,
      pngquant: true,
      verbose: true
    }))
    .pipe(gulp.dest(path.build.img))
    .pipe(reload({stream: true}));
});

gulp.task('fonts:build', function () {
  gulp.src(path.src.fonts)
    .pipe(gulp.dest(path.build.fonts))
});

gulp.task('ajax:build', function () {
  gulp.src(path.src.ajax)
    .pipe(gulp.dest(path.build.ajax))
});

gulp.task('php:build', function () {
  gulp.src(path.src.php)
    .pipe(gulp.dest(path.build.php))
});

gulp.task('video:build', function () {
  gulp.src(path.src.video)
    .pipe(gulp.dest(path.build.video))
});

gulp.task('json:build', function () {
  gulp.src(path.src.json)
    .pipe(gulp.dest(path.build.json))
});

gulp.task('xml:build', function () {
  gulp.src(path.src.xml)
    .pipe(gulp.dest(path.build.xml))
});

gulp.task('sprite:build', function () {
  var spriteData =
    gulp.src('./src/sprite/*.*') // путь, откуда берем картинки для спрайта
      .pipe(spritesmith({
        imgName: 'sprite.png',
        cssName: 'sprite.scss',
        cssFormat: 'scss',
        algorithm: 'diagonal',
        imgPath: '../img/sprite.png'
      }));

  spriteData.img.pipe(gulp.dest(path.build.sprite)); // путь, куда сохраняем картинку
  spriteData.css.pipe(gulp.dest('./src/style/sprite/')); // путь, куда сохраняем стили
});


gulp.task('svgSprite:build', function () {
  return gulp.src('src/svg-sprites/*.svg')
    .pipe(svgSprite({
      shape: {
        spacing: {
          padding: 5
        }
      },
      mode: {
        css: {
          dest: "./",
          layout: "diagonal",
          sprite: 'svg-sprite.svg',
          bust: false,
          render: {
            scss: {
              dest: "../../src/style/sprite/svg_sprite.scss",
              template: "src/style/sprite/sprite-template.scss"
            }
          }
        }
      },
      variables: {
        mapname: "icons"
      }
    }))
    .pipe(gulp.dest('build/img'));
});

gulp.task('sprite', [
  'sprite:build'
]);


gulp.task('build', gulpSequence(
  [
    'svgSprite:build',
    'sprite:build'
  ],
  'image:build',
  'video:build',
  'style:build',
  [
    'js:build',
    'fonts:build',
    'ajax:build',
    'php:build',
    'json:build',
    'xml:build'
  ],
  'html:build'
));


gulp.task('watch', function () {
  watch([path.watch.html], function (event, cb) {
    gulp.start('html:build');
  });
  watch([path.watch.style], function (event, cb) {
    gulp.start('style:build');
  });
  watch([path.watch.js], function (event, cb) {
    gulp.start('js:build');
  });
  watch([path.watch.img], function (event, cb) {
    gulp.start('image:build');
  });
  watch([path.watch.fonts], function (event, cb) {
    gulp.start('fonts:build');
  });
  watch([path.watch.ajax], function (event, cb) {
    gulp.start('ajax:build');
  });
  watch([path.watch.php], function (event, cb) {
    gulp.start('php:build');
  });
  watch([path.watch.video], function (event, cb) {
    gulp.start('video:build');
  });
  watch([path.watch.json], function (event, cb) {
    gulp.start('json:build');
  });
  watch([path.watch.xml], function (event, cb) {
    gulp.start('xml:build');
  });
  watch([path.watch.sprite], function (event, cb) {
    gulp.start('sprite:build');
  });
  watch([path.watch.spriteSvg], function (event, cb) {
    gulp.start('svgSprite:build');
  });
});


gulp.task('default', gulpSequence('build', 'watch', 'webserver'));
