$(document).on('keyup keypress input change', '[data-non-validate]', function () {
  if ($(this).val().match(/[^0-9 \s]/g)) {
    $(this).val($(this).val().replace(/[^0-9 \s]/g, ''));
  }

  if ($(this).val() === "") {
    $(this).closest('.payment-form__input-box').removeClass('full');
  } else {
    $(this).closest('.payment-form__input-box').addClass('full');
  }
});

$(document).on("blur", "[data-validate]", function () {
  if ($(this).parent().hasClass("error") && $(this).val().length === 0) {
    $(this).parent().removeClass("error")
  }
});