var CstPopover = function (el, trigger) {
  var indexClick = 0;

  function elShow() {
    $(el).addClass('open');
    indexClick = 1;
  }
  function elHide() {
    $(el).removeClass('open');
    indexClick = 0;
  }

  function toggle() {
    if (indexClick === 0) {
      elShow();
    }
    else {
      elHide()
    }
  }

  this.initClick = function () {
    $(document).on('click', trigger, function () {
      toggle();
    });

    $(document).on('click', function (event) {
      if ($(event.target).closest(el).length){
        return;
      } else {
        elHide();
      }
      event.stopPropagation();
    });
  }


};

var checkDealPopover = new CstPopover('.payment-tooltip','.payment-tooltip__trigger');
checkDealPopover.initClick();