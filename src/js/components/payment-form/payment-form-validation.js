(function ($) {
  function FormValidation(options) {
    this.options = $.extend({
      dataRequired: 'validate',
      inputWrap: '.input-row',
      submitButton: '.submit-btn',
      event: 'keyup keypress input on change',
      errorClass: 'error',
      correctClass: 'correct',
      errorMessageBox: '.validation-helper',
      nextStep: '.next-step',
      successEvent: function () {
      }
    }, options);
    this.init();
  }

  var currentDate = new Date();
  var currentMonth = currentDate.getMonth() + 1;
  var currentYear = currentDate.getFullYear();
  var cardExpiration = {
    month: null,
    year: null
  };

  var validationMessages = {
    numberMessage: 'Card number should be allowed only numbers and 8 charcters',
    cvvMessage: 'CVV should be allowed only numbers and 3 characters',
    monthMessage: 'Please type a valid month',
    yearMessage: 'Card expiration date must be in future',
    expirationMessage: 'Card expiration date should not be more than 5 years from current date'

  };

  FormValidation.prototype = {
    init: function () {
      this.findElements();
      this.events();
    },

    findElements: function () {
      this.form = jQuery(this.options.holder);
      this.requiredItems = this.form.find('[data-' + this.options.dataRequired + ']');
      this.submitButton = this.form.find(this.options.submitButton);
      this.nextStep = this.form.find(this.options.nextStep);

    },

    events: function () {
      var self = this;
      self.requiredItems.off(self.options.event);
      self.requiredItems.on(self.options.event, function () {
        var item = jQuery(this);
        var val = item.val();
        self.validateInputs(item, val, self);
      });
      self.form.off('submit');
      self.form.on('submit', function () {
        self.requiredItems.trigger('change'); // need for first load page with not empty form item
        var result = self.checkValidate();
        if (!result) {
          return false
        }
      });

      self.nextStep.on('click', function () {
        self.requiredItems.trigger('change'); // need for first load page with not empty form item

        var result = self.checkInputs();

        if (!result) {
          return false
        } else {
          self.nextStep.parent().addClass('hide');
          self.form.find('[data-step="02"]').addClass('active');
        }
      });

    },

    validateInputs: function (item, value, self) {


      switch (item.data(this.options.dataRequired)) {
        case 'required' :
          if (value === "") {
            item.closest(this.options.inputWrap).addClass(this.options.errorClass).removeClass(this.options.correctClass);
          } else {
            item.closest(this.options.inputWrap).removeClass(this.options.errorClass).addClass(this.options.correctClass);
          }
          break;
        case 'data-investment' :
          if (value.match(/[^0-9 \s]/g)) {
            item.val(value.replace(/[^0-9 \s]/g, ''));
          }

          if (value === "") {
            item.closest(this.options.inputWrap).addClass(this.options.errorClass).removeClass(this.options.correctClass);
          } else {
            item.closest(this.options.inputWrap).removeClass(this.options.errorClass).addClass(this.options.correctClass);
          }
          break;
        case 'card-number' :
          var txtNumber = /^[0-9]{8}$/;
          if (value.match(/[^0-9 \s]/g)) {
            item.val(value.replace(/[^0-9 \s]/g, ''));
          }

          if (!txtNumber.test(value)) {
            item.closest(this.options.inputWrap).addClass(this.options.errorClass).removeClass(this.options.correctClass);
            item.closest('.payment-form__input-box').find('.validation-helper').text(validationMessages.numberMessage).show();
          } else {
            item.closest(this.options.inputWrap).removeClass(this.options.errorClass).addClass(this.options.correctClass);
            item.closest('.payment-form__input-box').find('.validation-helper').text('').hide();
          }


          break;
        case 'card-cvv' :
          var cvv = /^[0-9]{3}$/;

          if (value.match(/[^0-9 \s]/g)) {
            item.val(value.replace(/[^0-9 \s]/g, ''));
          }
          if (!cvv.test(value)) {
            item.closest(this.options.inputWrap).addClass(this.options.errorClass).removeClass(this.options.correctClass);
            item.closest('.payment-form__input-box').find('.validation-helper').text(validationMessages.cvvMessage).show();
          } else {
            item.closest(this.options.inputWrap).removeClass(this.options.errorClass).addClass(this.options.correctClass);
            item.closest('.payment-form__input-box').find('.validation-helper').text('').hide();
          }


          break;
        case 'card-month' :
          var txtMonth = /^[0-9]{2}$/,
            userMonth = parseInt(item.val());

          if (value.match(/[^0-9 \s]/g)) {
            item.val(value.replace(/[^0-9 \s]/g, ''));
          }
          if (!txtMonth.test(value) || item.val() > 12) {
            item.closest(this.options.inputWrap).addClass(this.options.errorClass).removeClass(this.options.correctClass);
            item.closest('.payment-form__input-box--two-el').find('.validation-helper').text(validationMessages.monthMessage).show();
          } else {
            // add card expiration month
            cardExpiration.month = userMonth;

            item.closest(this.options.inputWrap).removeClass(this.options.errorClass).addClass(this.options.correctClass);
            item.closest('.payment-form__input-box--two-el').find('.validation-helper').text('').hide();

            if (currentYear + 5 === cardExpiration.year && cardExpiration.year !== null && userMonth > currentMonth) {
              item.closest('.payment-form__input-box--two-el').find('.validation-helper').text(validationMessages.expirationMessage).show();
              return false;
            } else if (currentYear + 5 === cardExpiration.year && cardExpiration.year !== null && userMonth <= currentMonth) {
              item.closest('.payment-form__input-box--two-el').find('.validation-helper').text('').hide();
              self.form.find('[data-validate="card-year"]').closest(this.options.inputWrap).removeClass(this.options.errorClass).addClass(this.options.correctClass);
            }

          }

          break;
        case 'card-year' :
          var txtYear = /^[0-9]{2}$/,
            userYear = parseInt('20' + item.val());

          if (value.match(/[^0-9 \s]/g)) {
            item.val(value.replace(/[^0-9 \s]/g, ''));
          }
          if (!txtYear.test(value)) {
            item.closest(this.options.inputWrap).addClass(this.options.errorClass).removeClass(this.options.correctClass);
            item.closest('.payment-form__input-box--two-el').find('.validation-helper').text(validationMessages.expirationMessage).show();

          } else {
            // add card expiration month
            cardExpiration.year = userYear;

            if (userYear > currentYear + 5) {
              item.closest(this.options.inputWrap).addClass(this.options.errorClass).removeClass(this.options.correctClass);
              item.closest('.payment-form__input-box--two-el').find('.validation-helper').text(validationMessages.expirationMessage).show();
              return false;
            }
            if (userYear < currentYear) {
              item.closest(this.options.inputWrap).addClass(this.options.errorClass).removeClass(this.options.correctClass);
              item.closest('.payment-form__input-box--two-el').find('.validation-helper').text(validationMessages.yearMessage).show();
              return false;
            }

            if (currentYear + 5 === userYear && cardExpiration.month !== null && cardExpiration.month > currentMonth) {
              item.closest('.payment-form__input-box--two-el').find('.validation-helper').text(validationMessages.expirationMessage).show();
              item.closest(this.options.inputWrap).addClass(this.options.errorClass).removeClass(this.options.correctClass);
              return false;
            } else if (currentYear + 5 === userYear && cardExpiration.month !== null && cardExpiration.month <= currentMonth) {
              item.closest('.payment-form__input-box--two-el').find('.validation-helper').text('').hide();
              self.form.find('[data-validate="card-month"]').closest(this.options.inputWrap).removeClass(this.options.errorClass).addClass(this.options.correctClass);
            }

            item.closest(this.options.inputWrap).removeClass(this.options.errorClass).addClass(this.options.correctClass);
            item.closest('.payment-form__input-box--two-el').find('.validation-helper').text('').hide();
          }


          break;
      }
    },

    checkInputs: function () {
      var self = this;
      if (self.requiredItems.length !== self.form.find('.' + this.options.correctClass).length) {
        self.requiredItems.each(function () {
          var item = jQuery(this);
          var val = item.val();
          self.validateInputs(item, val);
        });
        return false
      } else {
        return true
      }
    },
    checkValidate: function () {
      var self = this;
      if (self.requiredItems.length !== self.form.find('.' + this.options.correctClass).length) {
        self.requiredItems.each(function () {
          var item = jQuery(this);
          var val = item.val();
          self.validateInputs(item, val);
        });
        return false
      } else {

        var checkInvest;
        $('[data-non-validate]').each(function () {
          if ($(this).val().length !== 0) {
            checkInvest = true;
          }
        });

        if (typeof self.options.successEvent === 'function' && checkInvest) {
          self.options.successEvent();
        }
        return true
      }
    }
  };

  // jquery plugin
  $.fn.formValidation = function (opt) {
    return this.each(function () {
      $(this).data('FormValidation', new FormValidation($.extend(opt, {holder: this})));
    });
  };
})(jQuery);
