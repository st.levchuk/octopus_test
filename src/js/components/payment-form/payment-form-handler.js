(function () {
  var form = $('#payment-form'),
    handlerUrl = form.data('handler-url');

  form.formValidation({
    dataRequired: 'validate',
    inputWrap: '.payment-form__input-box',
    submitButton: '.payment-form__btn',
    successEvent: function () {
      call();
      return false;
    }
  }).on('submit', function () {
  });

  function call() {
    var formContent = form.serialize();

    $.ajax({
      url: handlerUrl,
      type: "POST",
      dataType: "html",
      data: formContent,
      success: function (data) {
        alert('sent form to your url - success');
      },
      error: function () {
        alert('sent form to your url - error');
      }
    })
  }
})();


